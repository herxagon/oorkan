const fs = require('fs')
const DB = {}

module.exports = DB

DB.readFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('./todos.json', 'utf-8', (err, data) => {
      if(err) return reject(err)

      return resolve(data)
    })
  })

}

DB.writeFile = (path, data) => {
  fs.writeFile(path, JSON.stringify(data, null, '\t'), (err) => {
    if(err) throw err
  })
}
