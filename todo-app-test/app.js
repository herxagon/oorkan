const http = require('http')
const fs = require('fs')
const ejs = require('ejs')
const express = require('express');
const bodyParser = require('body-parser')

const app = express();

const DB = require('./database.js')
const todoPath = './todos.json'

// CREEATE SERVER
const server = http.createServer((req, res) => {

  const {method, url, headers} = req
  let checkFile = fs.existsSync('./tweets.json')

  console.log(req.url)
  if(url === '/favicon.ico') res.end()

  if(method === 'POST') {
    let body = []
    req.on('error', (err) => {
      console.error(err)
    }).on('data', (chunk) => {
      console.log(chunk)
      body.push(chunk)
    }).on('end', () => {
      body = Buffer.concat(body).toString()

      console.log(body)
      // body = JSON.parse(body)

    })
  } else if (method === 'GET') {
    return DB.readFile()
    .then((data) => {
      // console.log(data)
      let template = ejs.compile(todoPath)
      template(data)
      console.log(template(data).toString())
    })
  }
})
//LISTEN TO PORT

server.listen(3000);
console.log('you are listening to port 3000')
