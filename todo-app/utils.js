const fs = require('fs')
const JSONpath = './todos.json'

const Utils = {}

module.exports = Utils

Utils.postBody = (req) => {
  return new Promise ((resolve, reject) => {
    let body = []
    req.on('error', (err) => {
      console.error(err)
    }).on('data', (chunk) => {
      body.push(chunk)
    }).on('end', () => {
      body = Buffer.concat(body).toString()
      // console.log(body)
      return resolve(body)
    })
  })

}

Utils.bodyToJson = (req) => {
  return Utils.postBody(req)
  .then((body) => {
    let jsonObj = {}
    let item = body.split('=')[0]
    let value = body.split('=')[1].trim()
    jsonObj = {item: value}
    return Promise.resolve(jsonObj)
  })

}
Utils.writeFile = (file, data) => {
  fs.writeFile(file, JSON.stringify(data, null, '\t'), (err) => {
    if(err) throw err
  })
}

Utils.readFile = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf-8', (err, data) => {
      if(err) throw err
      return resolve(data)
    })
  })
}


Utils.addTodos = (req) => {
  let bodyItems
  return Utils.bodyToJson(req)
  .then((item) => {
    bodyItems = item
    return bodyItems
  })
  .then(() => Utils.readFile(JSONpath))
  .then((fileData) => {
    let itemsObj = {}
    if(!fileData){
      itemsObj = {'todos': []}
    } else {
      itemsObj = JSON.parse(fileData)
    }

    let oldItems = itemsObj.todos
    let newItems = oldItems.concat(bodyItems)
    let newItemsObj = {'todos': newItems}

    return newItemsObj
  })
  .then((newObj) => {
    return Utils.writeFile(JSONpath, newObj)
  })
}

Utils.deleteTodos = (req, todo) => {
  return Utils.readFile(JSONpath)
  .then((data) => {
    let itemExists = false

    if(data){
      let fileTodos = JSON.parse(data.toString())
      fileTodos.todos = fileTodos.todos.filter((delTodo) => {
        if (delTodo.item != todo) {
          return true
        }else {
          itemExists = true
          return false
        }
      })
      if(itemExists){
        Utils.writeFile(JSONpath, fileTodos)
      }
      return Promise.resolve(itemExists)
    }
  })
}
