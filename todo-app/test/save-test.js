// const mocha = require('mocha')
const assert = require('assert')
const TodoList = require('../models/todo-list.js')


describe('saving records', () => {
  it('saves record to db', (done) =>{ // PASSING THE DONE AS PARAMETER

    let todoList = new TodoList({
      item: 'do something'
    })

    todoList.save() // SAVING THE NEW MODEL TO DB
    .then(() => {
      assert(todoList.isNew === false)
      done() // TELLING THE PROGRAM THAT WE ARE DONE
    })
    .catch((err) =>{
      console.log('error happened', err)
    })
  })

})
