const mongoose = require('mongoose')


before((done) => { // CONNECT DATABASE BEFORE SAVING DATA
  mongoose.connect('mongodb://localhost/test')

  mongoose.connection.once('open', () => {
    console.log('MongoDB connected')
    done()
  }).on('error', (err) => {
    console.log('connection error', err)
  })

})
