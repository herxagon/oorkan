window.onload = () => {

  const loadTodos = (todos) => {
    let outputs = ''
    data.forEach((todo) => {
      outputs += `
        <ul>
          <% for(let i = 0; i < data.length; i++) { %>
            <li><%= data[i].item %></li>
          <% } %>
        </ul>
      `
    })
    return outputs
  }

  const getTodos = () => {
    return new Promise((resolve, reject) => {
      console.log('blah')
      let xhr = new XMLHttpRequest()
      xhr.open('GET', 'todos.json', true)
      xhr.send()
      let resTodos = ''
      xhr.onreadystatechange = function (){
        if(xhr.readyState === 4) {
          if(xhr.status === 200 || xhr.status == 0) {
            resTodos = JSON.parse(xhr.responseText);
            const container = document.getElementById('todo-list')
            container.innerHTML = loadTodos(resTodos)
          }
        }
      }

    })

  }
  return getTodos()
  .then(todos => {
    // console.log(JSON.stringify(todos, null, '\t'))
  })


}
