const express = require('express');
let app = express();
let todoCtrl = require('./controllers/todo-ctrl.js')
//SET-UP TEMPLATE ENGINE
// console.log(express().request.params)
app.set('view engine', 'ejs');

// STATIC FILES

// app.use(express.static('./public'));
app.use(express.static('./'))

// FIRE CONTROLLERS

todoCtrl(app)
// console.log(app.locals.settings['view'].toString())

//LISTEN TO PORT

app.listen(3000);
console.log('you are listening to port 3000')
