const Utils = require('../utils')
const todoJSON = require('../todos.json')
const JSONpath = './todos.json'

module.exports = function(app) {

  // METHODS AND ROUTS

  app.get('/todo', (req, res) =>{
    return Utils.readFile(JSONpath)
    .then((todos) => {
      let data = todoJSON.todos
      res.render('todo', {data})
    })
    .catch((err) => {
      console.log('error happened', err)
    })
    res.end()
  });

  app.post('/todo', (req, res) =>{
    return Utils.addTodos(req)
    .then(() => {
      res.end()
      })
      .catch((err) => {
        console.log('error happened', err)
    })
  });

  app.delete('/todo/:delTodo', (req, res) => {
    // console.log(req.params.delTodo)
    let delTodo = req.params.delTodo
    // console.log(delTodo)
    return Utils.deleteTodos(req, delTodo)
    .then(() => {
      return Utils.readFile(JSONpath)
      .then((todos) => {
        let data = todoJSON.todos
        res.render('todo', {data})
        res.end()
      })
    })
    .catch((err) => {
      console.log('error happened', err)
    })
  });
};
