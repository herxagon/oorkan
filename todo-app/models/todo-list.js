const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// SCHEMA
const todoListSchema = new Schema({
  item: String
});

// MODEL
const todoList = mongoose.model('todo-list', todoListSchema);

module.exports = todoList;
