// So lets start with nodejs's process module, which is available in all nodejs modules
// in global object. This object is giving us ability to work with current process instance

// console.log(process.mainModule.filename);

// the things that we can do with the process module
// for example are: interacting with IO stream, getting variables from cmd interface etc..

// we are accessing the terminal arguments with argv property of process object

//console.log(process.argv);

// So here we can see that the process.argv variable is an array which is containing
// the path to node and the path to app.js file

//console.log(process.argv0); // Prints node

// The process.argv0 property stores a read-only copy
// of the original value of argv[0] passed when Node.js starts.


// Now if we'll give some arguments to out command
// like 'node app --firstname John --lastname Doe', we'll see that our array
// is now enlarge and contains the firstname, lastname keys and their values
// in primitive array way

function fullname(firstname, lastname) {
    var fname_index = process.argv.indexOf(firstname);
    var lname_index = process.argv.indexOf(lastname);

    return (fname_index != -1 && lname_index != -1) ? `The fullname of the user you just entered is ${process.argv[fname_index + 1]} ${process.argv[lname_index + 1]}` : 'There\'s a missing argument';
}

console.log(fullname('--firstname', '--lastname'));
