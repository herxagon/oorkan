var hello = 'hello world!';

// in browser when we print window.hello it will show the "hello world!" string

// unlike the browser in nodejs the global.hello will print undefined

// console.log(global.hello); // Logs undefind

// Nodejs introduces backtick characters strings ``
// This strings allow us to use variable names right within the string

// console.log(`I just said ${hello}`); // Prints 'I just said hello world' string

// So actually this is similar to php, where you can do the same by this way
// <?php $a = 'hello'; echo "I said ${a}"; ?> The important thing here is to remember
// that this will not work with single quotes notation, so in PHP the double quotes should
// be used for this

// console.log(__dirname); // Prints the full path to the dir in which the file is located
// console.log(__filename); // Prints the full path to the file which we are working with

// Other nodejs tool which is available for us globally is require() function
// which is standing for connect our current module with another nodejs modules

var path = require("path"); // The 'path' module is available to use with out default nodejs installation

// console.log(path.basename(__filename)); // This will plug the filename only
// from our filename fullpath

// When running a file with .js extension we can actually
// skip the .js extension, because the node will find that file for us
// so instead of running 'node global.js' we can actually run 'node global'


