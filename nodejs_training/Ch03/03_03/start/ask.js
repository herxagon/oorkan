var console = require('better-console');

// Another powerful tool of process module is that it can deal with standard IO
// so we can communicate with the process while it's runnin

// we already have used the standard output object
// the console.log uses it to print the stuff to the console for example

// process.stdout.write('hello world! \n');

// Lets now create some game

var questions = [
    'What\'s your name?',
    'How old are you?'
];

function ask(question) {
    return process.stdout.write(question);
}

// lets add a listener for the data input on standard input object
process.stdin.on('data', function() {
    let data = JSON.parse(JSON.stringify(arguments[0]));
    console.warn(data.type + '\n', (new Buffer(data.data)).toString());
});

ask(questions[0]);

// module.exports = {
//     ask: ask(questions[0])
// }

