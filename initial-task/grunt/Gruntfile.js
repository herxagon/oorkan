module.exports = function(grunt){

  //project config
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    'sass': {
      'options': {
        'sourcemap': 'none',
        'style':'compressed'
      },
      'build': {
        'src':'../scss/**/*.scss',
        'dest':'.././css',
        'expand':true,
        'ext':'.css',
        'flatten':true
      }
    },
    'imagemin': {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'img',
          src: ['../**/*.{png,jpg,gif}'],
          dest: '../img/opt/'
        }]
      }
    }
  });

  //Load plugins
  grunt.loadNpmTasks('grunt-sass') // must do, otherwise says: local NPM module not found
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  // grunt.loadNpmTasks('grunt-sass-compile-imports');

  // Default tasks
  grunt.registerTask('default', ['sass', 'imagemin'])
  // grunt.registerTask('run', function(){
  //   console.log('i am running')
  // })
};

// module.exports = function(grunt) {
//
//   // Project configuration.
//   require('load-grunt-tasks')(grunt);
//
//   grunt.initConfig({
//     pkg: grunt.file.readJSON('package.json'),
//     'sass': {
//       'options': {
//         'sourcemap': 'none',
//         'style':'compressed'
//       },
//       'build': {
//         'src':'scss/**/*.scss',
//         'dest':'../',
//         'expand':true,
//         'ext':'.css',
//         'flatten':true
//       }
//     },
//     'concat': {
//       options: {
//         separator: ';',
//       },
//       dist: {
//         src: ['js/originals/**/*.js'],
//         dest: 'js/compiled/main.js',
//       }
//     },
//     'uglify': {
//       options: {},
//       my_target: {
//         files: {
//           'js/compiled/main.min.js': ['js/compiled/main.js']
//         }
//       }
//     },
  //   'imagemin': {
  //     dynamic: {
  //       files: [{
  //         expand: true,
  //         cwd: '../img/origin/',
  //         src: ['**/*.{png,jpg,gif}'],
  //         dest: '../img/opt/'
  //       }]
  //     }
  //   }
  // });
//
//
//   // Default task(s).
//   grunt.registerTask('default', ['sass', 'concat', 'uglify', 'imagemin']);
//
// };
