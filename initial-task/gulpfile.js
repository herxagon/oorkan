const gulp = require('gulp');
const sass = require('gulp-sass')

gulp.task('scss', function(){
  let watcher = gulp.watch('scss/**/*.scss')
  watcher.on('change', function(event){
    console.log('file ', event.path, ' was changed')
    // console.log(watcher)
    return gulp.src('scss/style.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('css'))
  })
})

gulp.task('default', ['scss'])



// gulp.task('scss', function() {
//   return gulp.src('scss/style.scss')
//   .pipe(sass().on('error', sass.logError))
//   .pipe(gulp.dest('css'))
// });


// gulp.task('watch', function(){
//   let watcher = gulp.watch('scss/**/*.scss')
//   watcher.on('change', function(event){
//     console.log(' the file ' + event.path + ' was changed')
//   })
// })
//
//
// gulp.task('default', function(){
//   console.log('running correctly')
// })

// gulp.task('html', function() {
//   return gulp.scr('client/templates/*.pug')
//   .pipe(pug())
//   .pipe(gulp.dest('build/html'))
// });
//
// gulp.task('css', function() {
//   return gulp.src('client/templates/*.less')
//   .pipe(less())
//   .pipe(minifyCss())
//   .pipe(gulp.dest('build/css'))
// });
//
// gulp.task('default', ['html', 'css']);
