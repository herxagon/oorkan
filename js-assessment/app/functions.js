exports = typeof window === 'undefined' ? global : window;

exports.functionsAnswers = {
  argsAsArray: function(fn, arr) {
    // console.log(fn)
    // console.log(arr)

    // console.log(fn.apply(fn, arr))
    return fn.apply(fn, arr)
  },

  speak: function(fn, obj) {
    // console.log(fn.call(obj))
    // console.log(fn)
    return fn.call(obj)
  },

  functionFunction: function(str) {
    console.log(str)
    return function(str){
      console.log(str + str)
      str + str
    }
  },

  makeClosures: function(arr, fn) {

  },

  partial: function(fn, str1, str2) {

  },

  useArguments: function() {

  },

  callIt: function(fn) {

  },

  partialUsingArguments: function(fn) {

  },

  curryIt: function(fn) {

  }
};
