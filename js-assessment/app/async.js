exports = typeof window === 'undefined' ? global : window;

exports.asyncAnswers = {
  async: function(value) {
    return new Promise ((resolve, reject) => {
      if (!value) return reject(value)
      return resolve(value)
    })
  },

  manipulateRemoteData: function(url) {

    function readFile(file) {
      return new Promise((resolve, reject) => {

        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        let allText
        let peopleArr = []
        rawFile.onreadystatechange = function (){
          if(rawFile.readyState === 4) {
            if(rawFile.status === 200 || rawFile.status == 0) {
              allText = JSON.parse(rawFile.responseText);

            }
          }
        }
        rawFile.send(null);
        allText.people.map((person) => {
          return peopleArr.push(person.name)
        })
        return resolve(peopleArr.sort())
      }) //promise end
    }
    return readFile(url)
  }

};
