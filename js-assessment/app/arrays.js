exports = typeof window === 'undefined' ? global : window;

exports.arraysAnswers = {
  indexOf: function(arr, item) {

    return arr.indexOf(item);
    // for(let i = 0; i < arr.length; i++) {
    //   if(arr[i] == item) return i
    //   -1
    // }

  },

  sum: function(arr) {
    let sum = 0

    for(let i = 0; i < arr.length; i++) {
      sum += arr[i]
    }
    return sum
  },

  remove: function(arr, item) {
    let filteredArr = []
    for (i = 0; i < arr.length; i++) {
      if(arr[i] !== item) {
        filteredArr.push(arr[i])
      }
    }
    // console.log(filteredArr)
    return filteredArr
    // let filtered = arr.filter((item) => {
    //   items === item
    // })
    // return filtered
  },

  removeWithoutCopy: function(arr, item) {
    let len = arr.length
    let i
    for (i = 0; i < len; i++) {
      if (arr[i] == item) {
        arr.splice(i, 1)
        i--
        len--
      }
      // console.log(item)
    }
    return arr
  },

  append: function(arr, item) {
    arr.push(item)
    return arr
  },

  truncate: function(arr) {
    arr.pop()
    return arr
  },

  prepend: function(arr, item) {
    arr.unshift(item)
    return arr
  },

  curtail: function(arr) {
    arr.shift()
    return arr
  },

  concat: function(arr1, arr2) {
    let newArr = arr1.concat(arr2)
    return newArr
  },

  insert: function(arr, item, index) {
    for (var i = 0; i < arr.length; i++) {
      if (i == index) {
        arr.splice(i, 0, item)
      }
    }
    return arr
  },

  count: function(arr, item) {
    let count = 0
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == item) {
        count++
      }
    }
    return count
  },

  duplicates: function(arr) {
    let dupes = []
    for(let i = 0; i < arr.length; i++) {
      (arr.lastIndexOf(arr[i]) != i && dupes.indexOf(arr[i]) == -1) ? dupes.push(arr[i]) : '';
    }
    return dupes
  },

  square: function(arr) {
    let itemSquared = []
    for (var i = 0; i < arr.length; i++) {
      itemSquared.push(arr[i] * arr[i])
    }
    return itemSquared
  },

  findAllOccurrences: function(arr, target) {
    let occurances = []
    for (var i = 0; i < arr.length; i++) {
      if(arr[i] == target) {
        occurances.push(i)
      }
    }
    return occurances
  }
};
